#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, get_prediction
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract
import sys

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # netflix_eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual( w.getvalue(), "10040:\n3.3\n3.4\n3.7\n0.90\n")

    def test_eval_2(self):
    	r = StringIO("10:\n1952305\n1531863\n")
    	w = StringIO()
    	netflix_eval(r, w)
    	self.assertEqual( w.getvalue(), "10:\n3.3\n3.2\n0.23\n")
    
    def test_eval_3(self):
        r = StringIO("10007:\n1204847\n2160216\n248206\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual( w.getvalue(), "10007:\n2.9\n3.2\n3.0\n1.56\n")


    # ----
    # get_prediction
    # ----

    def test_get_prediction_1(self):
    	v = get_prediction (10040, 2417853)
    	self.assertEqual(round(v,1), 3.3)

    def test_get_prediction_2(self):
    	v = get_prediction (10, 1952305)
    	self.assertEqual(round(v,1), 3.3)

    def test_get_prediction_3(self):
        v = get_prediction (10007, 1204847)
        self.assertEqual(round(v,1), 2.9)
# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""

