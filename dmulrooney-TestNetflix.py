#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, predicted_val
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----
    #these unit tests test the netflix_eval function to check if the expected RMSE value is being returned
    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.3\n3.4\n3.7\n0.92\n")
    
    def test_eval_2(self):
        r = StringIO("12077:\n90789\n208541\n71817\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "12077:\n3.7\n3.6\n3.5\n1.17\n")
    
    def test_eval_3(self):
        r = StringIO("12110:\n1514244\n909934\n161953\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "12110:\n3.7\n3.6\n3.5\n1.78\n")
    
    def test_eval_4(self):
        r = StringIO("12182:\n2390994\n2079090\n398192\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "12182:\n3.7\n3.9\n4.0\n0.95\n")
    
    def test_eval_5(self):
        r = StringIO("9997:\n2179700\n1347835\n765578\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "9997:\n3.7\n3.8\n3.7\n1.0\n")
    #these unit tests test the predicted_val function to ensure they are returned the expected predicted rating for the movie and custoemr inputted to it
    def test_eval_6(self):
        self.assertEqual(predicted_val((2336448,5085)), 4.1)

    def test_eval_7(self):
        self.assertEqual(predicted_val((457005,5085)), 3.7)

    def test_eval_8(self):
        self.assertEqual(predicted_val((2326571,1000)), 3.4)
    
    def test_eval_9(self):
        self.assertEqual(predicted_val((195342,1988)), 4.0)


# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
