#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, netflix_pred
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        _ = netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.3\n3.42\n3.78\n0.92\n")


    def test_eval_actual_pred(self):
        r = StringIO("10019:\n22036\n1126085\n487154\n261890\n204512\n2350988\n943653\n1725053\n")
        w = StringIO()
        rms = netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10019:\n3.56\n2.79\n3.72\n3.34\n4.02\n3.96\n3.6\n3.14\n0.93\n")
    def test_eval_rmsLess1(self):
        r=StringIO("1:\n30878\n2647871\n1283744\n2488120\n317050\n1904905\n1989766\n14756\n1027056\n1149588\n1394012\n1406595\n2529547\n1682104\n2625019\n2603381\n1774623\n470861\n712610\n1772839\n1059319\n2380848\n548064\n")
        w = StringIO()
        rms = netflix_eval(r, w)
        self.assertTrue(rms<1.)  

    def test_eval_netflix_pred1(self):
        rate = netflix_pred(100,200,2)
        self.assertEqual(
            rate, 1)

    def test_eval_netflix_pred2(self):
        rate = netflix_pred(10019,22036,2)
        self.assertEqual(
            rate, 1)

    def test_eval_netflix_pred3(self):
        rate = netflix_pred(109,36,0)
        self.assertEqual(
            rate, 0)

    def test_eval_netflix_pred4(self):
        rate = netflix_pred(22036,12,3.401)
        self.assertEqual(
            rate, 3.5365)

    def test_eval_netflix_pred5(self):
        rate = netflix_pred(22036,10019,2)
        self.assertEqual(
            rate, 3.42)        

# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
